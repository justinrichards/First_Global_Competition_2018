/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DigitalChannel;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcontroller.external.samples.SensorDIO;


@TeleOp(name="FGC_Jamaica_Code", group="Jamaica-Fgc")
public class Jamaica_FGC_2018 extends LinearOpMode {

    // Declare Motors and sensors
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotor leftDrive = null;
    private DcMotor rightDrive = null;
    private DcMotor leftdrive2= null;
    private DcMotor rightdrive2= null;
    private DcMotor winddrive = null;
    private DigitalChannel limit = null;
    private DcMotor lift_motor = null;

    //Declare global variables
    int position = 0;
    int mid= 0;


    @Override
    public void runOpMode() {
        telemetry.addData("Status", "Initialized");

        // Initialize the hardware variables. and link it the code to the sensors and motors
        leftDrive  = hardwareMap.get(DcMotor.class, "M1");
        rightDrive = hardwareMap.get(DcMotor.class, "Exm1");
        leftdrive2  = hardwareMap.get(DcMotor.class, "Exm2");
        rightdrive2 = hardwareMap.get(DcMotor.class, "Exm3");
        winddrive= hardwareMap.get(DcMotor.class,"W1");
        limit = hardwareMap.get(DigitalChannel.class, "Limit");
        lift_motor= hardwareMap.get(DcMotor.class,"L1");

        // Motors and sensors prior configurations
        leftDrive.setDirection(DcMotor.Direction.FORWARD);
        rightDrive.setDirection(DcMotor.Direction.REVERSE);
        leftdrive2.setDirection(DcMotor.Direction.FORWARD);
        rightdrive2.setDirection(DcMotor.Direction.REVERSE);
        winddrive.setMode(DcMotor.RunMode.RUN_TO_POSITION);
//        limit.setMode(DigitalChannel.Mode.INPUT);

        //Variable assigning
//        mid = lift_motor.getCurrentPosition();

        // wait fot the user to press start button then run below
        waitForStart();
        runtime.reset();

        // run until the end of the match (driver presses STOP)
        while (opModeIsActive()) {
            telemetry.addData("Status ", "Run Time: " + runtime.toString());
            telemetry.update();
            //driving configuration
         //   driving();

            //windmill configuration
            windmill_turn();

            //lift configuration
          //  Lift_Function();

            //telemetry.update();
        }
    }

    private void driving(){
        // Setup a variable for each drive wheel to save power level for telemetry
        double leftPower;
        double rightPower;

        //assign the values to the motor power variables
        leftPower  = -gamepad1.left_stick_y *0.25;
        rightPower = -gamepad1.right_stick_y *0.25;

        // Send calculated power to wheels
        leftDrive.setPower(leftPower);
        rightDrive.setPower(rightPower);
        leftdrive2.setPower(leftPower);
        rightdrive2.setPower(rightPower);

        // Show the joystick values for the
        telemetry.addData("Motors: ", "Left Wheel -(%.2f)%, Right Wheel-(%.2f)%", leftPower*100, rightPower*100);
    }
    private void windmill_turn() {
        //variables declaration
        position = winddrive.getCurrentPosition();

        //Receive whether the A is press or not for windmill
        boolean millpress = gamepad1.a;

        //accept value if the right trigger is pressed
        boolean spin = gamepad1.right_bumper;

        //If the windmill button is pressed spin for three rotations
        if (millpress) {
            winddrive.setTargetPosition(position +(288 * 3));
            winddrive.setPower(0.5);
        }
        //If extra spin is needed and spin button is pressed windmill spin a bit
        if (spin ) {
            winddrive.setTargetPosition(position + 144);
            winddrive.setPower(0.25);
        }
        //show the position of the windmill on screen
        telemetry.addData("Windmill Position: ", "%d  ", position);
    }

    private void Lift_Function() {
        //Variables and receiving button calls
        boolean fly = gamepad1.dpad_up;
        boolean fall = gamepad1.dpad_down;
        int lift_motor_pos = lift_motor.getCurrentPosition();

        //If dpad up is pressed then allow the lift to rise until the switch is reached
        if (fly) {
            if (limit.getState() == true || lift_motor_pos <= mid) {
                // switch not reach
                lift_motor.setPower(0.5);
            }
            if(!limit.getState() && lift_motor_pos < mid){
                //switch reached
                lift_motor.setPower(0);
            }
        }else{
            lift_motor.setPower(0);
        }

        //If dpad down is pressed then allow the lift to rise until the switch is reached
        if(fall){
            if(limit.getState() == true || lift_motor_pos >= mid){
                // switch not reach
                lift_motor.setPower(-0.5);
            }
            if(!limit.getState() && lift_motor_pos > mid){
                //switch reached
                lift_motor.setPower(0);
            }
        }else{
            lift_motor.setPower(0);
        }
        // Show the position of the
        telemetry.addData("Lift Position: ", "%d |Midpoint: %d",lift_motor_pos, mid);

    }

}